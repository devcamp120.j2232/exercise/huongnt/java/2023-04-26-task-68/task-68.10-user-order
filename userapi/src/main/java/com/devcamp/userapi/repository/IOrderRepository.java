package com.devcamp.userapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.userapi.model.COrder;

public interface IOrderRepository extends JpaRepository<COrder, Long>{
    
}
